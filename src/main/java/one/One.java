package one;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class One {

    public static void main(String[] args) {

        // ----------- 1 -----------
        List<Person> persons = List.of(
                new Person("Clinton", "M", 10711),
                new Person("Jayna", "F", 23003),
                new Person("Eben", "M", 80063),
                new Person("April", "F", 72250),
                new Person("Braylen", "M", 33444),
                new Person("Valarie", "F", 61147),
                new Person("Kody", "M", 26020),
                new Person("Cate", "F", 79082),
                new Person("Will", "M", 15023),
                new Person("Ina", "F", 94663)
        );

        // ----------- 1.1 -----------
        System.out.println(
                persons
                        .stream()
                        .collect(Collectors.groupingBy(
                                Person::getGender,
                                Collectors.averagingDouble(Person::getSalary)
                        ))
        );

        // ----------- 1.2 -----------
        persons
                .stream()
                .sorted(Comparator.comparing(Person::getSalary).reversed())
                .map(Person::getName)
                .findFirst()
                .ifPresent(System.out::println);

        // ----------- 1.3 -----------
        persons
                .stream()
                .sorted(Comparator.comparing(Person::getSalary))
                .map(Person::getName)
                .findFirst()
                .ifPresent(System.out::println);
    }
}
