package four;

import java.util.stream.IntStream;

public class Four {

    public static void main(String[] args) {

        Thread primeThread0to350000 = new Thread(() -> {
            System.out.println(
                    IntStream
                            .rangeClosed(2, 350000)
                            .filter(Four::isPrime)
                            .count()
            );
        });
        Thread primeThread350001to500000 = new Thread(() -> {
            System.out.println(
                    IntStream
                            .rangeClosed(350001, 500000)
                            .filter(Four::isPrime)
                            .count()
            );
        });

        primeThread0to350000.start();
        primeThread350001to500000.start();

    }

    public static boolean isPrime(int number) {

        return IntStream.rangeClosed(2, (int) (Math.sqrt(number)))
               .allMatch(n -> number % n != 0);
    }

}
