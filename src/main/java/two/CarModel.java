package two;

public enum CarModel {
    ELECTRIC,
    PETROL,
    DIESEL
}
