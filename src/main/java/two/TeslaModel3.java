package two;

public class TeslaModel3 implements Car {
    private double price;
    private double tankSize;

    public TeslaModel3(double price, double tankSize) {
        this.price = price;
        this.tankSize = tankSize;
    }

    @Override
    public void fillUp() {

        double fillUpCost = price * tankSize;

        System.out.printf(
                "The cheapest option, but still expensive these days. You lost %.2f kr.\n", fillUpCost
        );
    }
}
