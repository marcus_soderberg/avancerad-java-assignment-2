package two;

public class HondaCivicTypeR implements Car {
    private double price;
    private double tankSize;

    public HondaCivicTypeR(double price, double tankSize) {
        this.price = price;
        this.tankSize = tankSize;
    }

    @Override
    public void fillUp() {

        double fillUpCost = price * tankSize;

        System.out.printf(
                "Not as expensive av diesel. You lost %.2f kr.\n", fillUpCost
        );
    }
}
