package two;

public class Two {
    public static void main(String[] args) {

        CarFactory carFactory = new CarFactory();

        Car expensiveCar = carFactory.createCar(CarModel.DIESEL);
        expensiveCar.fillUp();

        System.out.println("------");

        Car lessExpensiveCar = carFactory.createCar(CarModel.PETROL);
        lessExpensiveCar.fillUp();

        System.out.println("------");

        Car evenLessExpensiveCar = carFactory.createCar(CarModel.ELECTRIC);
        evenLessExpensiveCar.fillUp();
    }
}
