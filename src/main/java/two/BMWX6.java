package two;

public class BMWX6 implements Car {
    private double price;
    private double tankSize;

    public BMWX6(double price, double tankSize) {
        this.price = price;
        this.tankSize = tankSize;
    }

    @Override
    public void fillUp() {

        double fillUpCost = price * tankSize;

        System.out.printf(
                "Expensive! You lost %.2f kr. :(\n", fillUpCost
        );
    }
}
