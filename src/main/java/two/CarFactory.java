package two;

public class CarFactory {

    public Car createCar(CarModel carModel) {

        return switch (carModel) {
            case ELECTRIC -> new TeslaModel3(2.54, 75);
            case PETROL -> new HondaCivicTypeR(18.79, 46);
            case DIESEL -> new BMWX6(21.27, 85);
        };
    }
}
