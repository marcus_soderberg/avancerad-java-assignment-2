package three;

import java.util.List;
import java.util.regex.Pattern;

public class Three {

    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("(?i).*[aeiouy].*[aeiouy].*");
        List<String> words = List.of(
                "breakfast",
                "basis",
                "at",
                "cast",
                "escape",
                "major",
                "redundancy",
                "spend",
                "seize",
                "guide",
                "shorts",
                "dark",
                "grind",
                "mature"
        );
        words
                .stream()
                .filter(word -> pattern.matcher(word).find())
                .forEach(System.out::println);
    }

}
